using DeliveryFoodAuth.DAL.Core;
using DeliveryFoodAuth.DAL.Entities;

namespace DeliveryFoodAuth.DAL.Contracts;

public interface IUserRepository : IRepository<User, int>, IAsyncRepository<User, int>
{
    Task<User> GetByEmailAsync(string email);
    Task<bool> IsUserExistsAsync(string email); 
    Task<bool> IsUserActiveAsync(string email);
}