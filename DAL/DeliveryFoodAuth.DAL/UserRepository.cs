using DeliveryFoodAuth.DAL.Contracts;
using DeliveryFoodAuth.DAL.Core;
using DeliveryFoodAuth.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DeliveryFoodAuth.DAL;

public class UserRepository : BaseRepository<User, int>, IUserRepository
{
    public UserRepository(DeliveryFoodAuthContext dataContext) : base(dataContext)
    {
    }

    public async Task<User> GetByEmailAsync(string email)
    {
        var user = await _dbContext.Users.FirstOrDefaultAsync(e => e.Email == email);
        return user;
    }

    public async Task<bool> IsUserExistsAsync(string email)
    {
        var user = await _dbContext.Users.FirstOrDefaultAsync(e => e.Email == email);
        return user is not null;
    }
    
    public async Task<bool> IsUserActiveAsync(string email)
    {
        var user = await _dbContext.Users.FirstOrDefaultAsync(e => e.Email == email);
        if (user is null)
            throw new NullReferenceException("user is null");
        
        return user.IsActive;
    }
}