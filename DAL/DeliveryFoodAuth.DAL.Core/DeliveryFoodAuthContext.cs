using DeliveryFoodAuth.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DeliveryFoodAuth.DAL.Core;

public class DeliveryFoodAuthContext : DbContext
{
    public DeliveryFoodAuthContext(DbContextOptions<DeliveryFoodAuthContext> options) : base(options)
    {
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Role> Roles { get; set; }
}