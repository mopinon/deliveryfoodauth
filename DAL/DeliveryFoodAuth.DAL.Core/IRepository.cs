namespace DeliveryFoodAuth.DAL.Core;

public interface IRepository<T, TId>
{
    T GetById(TId id);
    IEnumerable<T> ListAll();
    T Add(T entity);
    void Update(T entity);
    void Delete(T entity);
}