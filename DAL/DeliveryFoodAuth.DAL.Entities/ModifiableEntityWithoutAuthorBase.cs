namespace DeliveryFoodAuth.DAL.Entities;

public abstract class ModifiableEntityWithoutAuthorBase : EntityBase
{
    public DateTime CreatedDate { get; set; }
    public DateTime ModifiedDate { get; set; }
}