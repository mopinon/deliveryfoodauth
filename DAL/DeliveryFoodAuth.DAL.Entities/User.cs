namespace DeliveryFoodAuth.DAL.Entities;

public class User : ModifiableEntityWithoutAuthorBase
{
    public string Email { get; set; }
    public string HashPassword { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public Role Role { get; set; }
    public int RoleId { get; set; }
    public bool IsActive { get; set; }
}