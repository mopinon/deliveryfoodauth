docker build -t mopin/delivery-food-auth -f ./Web/DeliveryFoodAuth.Web/Dockerfile .
docker push  mopin/delivery-food-auth
ssh otus <<'ENDSSH'
    cd /home/delivery-food-auth/
    docker pull mopin/delivery-food-auth
    docker-compose up -d
ENDSSH