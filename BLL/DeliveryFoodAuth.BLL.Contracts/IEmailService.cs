namespace DeliveryFoodAuth.BLL.Contracts;

public interface IEmailService
{
    public Task<SendEmailResult> SendEmailAsync(string email, string subject, string message);
}

public enum SendEmailResult
{
    Ok,
    InvalidEmail
}