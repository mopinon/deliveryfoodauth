namespace DeliveryFoodAuth.BLL.Contracts;

public interface IHashService
{
    public string HashPasswordRfc2898(string password);
    public bool VerifyHashedPasswordRfc2898(string hashedPassword, string password);
}