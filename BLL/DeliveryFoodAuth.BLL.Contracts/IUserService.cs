using DeliveryFoodAuth.BLL.Dto;

namespace DeliveryFoodAuth.BLL.Contracts;

public interface IUserService
{
    Task<AddUserDTO> AddUserAsync(AddUserDTO addUserDTO);
    Task<RemoveUserDTO> RemoveUserAsync(RemoveUserDTO removeUserDTO);
    Task<GetUserDTO> GetUserAsync(int id);
    Task<GetUserDTO> GetUserByEmailAsync(string email);
    Task<IEnumerable<GetAllUseresDTO>> GetAllUserAsync();
    Task<UpdateUserDTO> UpdateUser(UpdateUserDTO updateUserDTO);
    Task<string> ConfirmEmail(string token);
    Task<string> Login(LoginRequest request);
    Task LoginWithToken(string token);
}