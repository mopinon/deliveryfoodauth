using System.Text;
using System.Text.Json;
using DeliveryFoodAuth.BLL.Contracts;
using RabbitMQ.Client;

namespace DeliveryFoodAuth.BLL;

public class RabbitMqService : IRabbitMqService
{
    public void SendMessage(object obj)
    {
        var message = JsonSerializer.Serialize(obj);
        SendMessage(message);
    }

    public void SendMessage(string message)
    {
        var factory = new ConnectionFactory { HostName = "37.230.114.216" };
        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare("Notify",
                false,
                false,
                false,
                null);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish("",
                "Notify",
                null,
                body);
        }
    }
}