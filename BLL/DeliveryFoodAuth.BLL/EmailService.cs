using DeliveryFoodAuth.BLL.Contracts;
using DeliveryFoodAuth.BLL.Dto;

namespace DeliveryFoodAuth.BLL;

public class EmailService : IEmailService
{
    private readonly IRabbitMqService _rabbitMqService;

    public EmailService(IRabbitMqService rabbitMqService)
    {
        _rabbitMqService = rabbitMqService;
    }

    public async Task<SendEmailResult> SendEmailAsync(string email, string subject, string message)
    {
        if (!email.IsValidEmail())
            return SendEmailResult.InvalidEmail;

        var sendEmailDto = new SendEmailDto
        {
            Email = email,
            Subject = subject,
            Message = message
        };
        
        _rabbitMqService.SendMessage(sendEmailDto);
        
        return SendEmailResult.Ok;
    }
}