using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace DeliveryFoodAuth.BLL;

public class JwtOptions
{
    public const string ISSUER = "DeliveryFoodServer"; // издатель токена
    public const string AUDIENCE = "DeliveryFoodClient"; // потребитель токена
    const string KEY = "DeliveryFoodKey";   // ключ для шифрации
    public const int LIFETIME = 10080; // время жизни токена - 1 минута
    public static SymmetricSecurityKey GetSymmetricSecurityKey()
    {
        return new SymmetricSecurityKey(Encoding.Unicode.GetBytes(KEY));
    }
}