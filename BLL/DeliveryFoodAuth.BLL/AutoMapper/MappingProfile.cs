using AutoMapper;
using DeliveryFoodAuth.BLL.Dto;
using DeliveryFoodAuth.DAL.Entities;

namespace DeliveryFoodAuth.BLL.AutoMapper;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<User, UpdateUserDTO>().ReverseMap();
        CreateMap<User, AddUserDTO>().ReverseMap();
        CreateMap<User, RemoveUserDTO>().ReverseMap();
        CreateMap<User, GetUserDTO>().ReverseMap();
        CreateMap<User, GetAllUseresDTO>().ReverseMap();
    }
}