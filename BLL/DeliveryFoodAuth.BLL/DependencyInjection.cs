using System.Reflection;
using DeliveryFoodAuth.BLL.Contracts;
using DeliveryFoodAuth.DAL;
using DeliveryFoodAuth.DAL.Contracts;
using DeliveryFoodAuth.DAL.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DeliveryFoodAuth.BLL;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = JwtOptions.ISSUER,
                    ValidateAudience = true,
                    ValidAudience = JwtOptions.AUDIENCE,
                    ValidateLifetime = false,
                    IssuerSigningKey = JwtOptions.GetSymmetricSecurityKey(),
                    ValidateIssuerSigningKey = true
                };
            });
        
        var defaultConnectionString = configuration.GetConnectionString("DeliveryFoodConnection");
        services.AddDbContext<DeliveryFoodAuthContext>(e => e.UseNpgsql(defaultConnectionString));

        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IUserService, UserService>();
        
        services.AddScoped<IEmailService, EmailService>();
        services.AddScoped<IJwtService, JwtService>();
        services.AddScoped<IHashService, HashService>();
        services.AddScoped<IRabbitMqService, RabbitMqService>();
        
        services.AddAutoMapper(Assembly.GetExecutingAssembly());

        return services;
    }
}