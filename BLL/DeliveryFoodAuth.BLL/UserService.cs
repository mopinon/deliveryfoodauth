using AutoMapper;
using DeliveryFoodAuth.BLL.Contracts;
using DeliveryFoodAuth.BLL.Dto;
using DeliveryFoodAuth.DAL.Contracts;
using DeliveryFoodAuth.DAL.Entities;

namespace DeliveryFoodAuth.BLL;

public class UserService : IUserService
{
    private readonly IEmailService _emailService;
    private readonly IHashService _hashService;
    private readonly IJwtService _jwtService;
    private readonly IMapper _mapper;
    private readonly IUserRepository _userRepository;

    public UserService(IUserRepository userRepository, IMapper mapper, IJwtService jwtService,
        IEmailService emailService,
        IHashService hashService)
    {
        _userRepository = userRepository;
        _mapper = mapper;
        _jwtService = jwtService;
        _emailService = emailService;
        _hashService = hashService;
    }

    public async Task<AddUserDTO> AddUserAsync(AddUserDTO addUserDTO)
    {
        var email = addUserDTO.Email.ToLower();
        var hashPassword = _hashService.HashPasswordRfc2898(addUserDTO.Password);
        var encodedJwt = _jwtService.Encode(email);

        var isUserExists = await _userRepository.IsUserExistsAsync(email);
        if (isUserExists)
        {
            var isUserActive = await _userRepository.IsUserActiveAsync(email);
            if (isUserActive)
                throw new InvalidOperationException("user is exists and active");

            var existingUser = await _userRepository.GetByEmailAsync(email);
            _mapper.Map(addUserDTO, existingUser);
            existingUser.HashPassword = hashPassword;
            existingUser.ModifiedDate = DateTime.UtcNow;

            await _userRepository.UpdateAsync(existingUser);
        }
        else
        {
            var user = _mapper.Map<User>(addUserDTO);
            user.CreatedDate = DateTime.UtcNow;
            user.ModifiedDate = user.CreatedDate;
            user.IsActive = false;
            user.Email = email;
            user.HashPassword = hashPassword;

            await _userRepository.AddAsync(user);
        }

        var host = Environment.GetEnvironmentVariable("APP_HOST") ?? "localhost";
        var port = Environment.GetEnvironmentVariable("APP_PORT") ?? "7136";
        var link = $"http://{host}:{port}/auth/confirmEmail/?token={encodedJwt}";
        // var message = await PageRenderer
        //     .RenderPageToHtmlStringAsync(HttpContext.RequestServices, PathToConfirmEmailPage, link);
        var message = link;
        // todo переделать на RabbitMq
        var sendEmailResult = await _emailService.SendEmailAsync(email.ToLower(), "Подтверждение регистрации", message);
        if (sendEmailResult == SendEmailResult.InvalidEmail)
            throw new Exception("invalid email");

        return addUserDTO;
    }

    public async Task<RemoveUserDTO> RemoveUserAsync(RemoveUserDTO removeUserDTO)
    {
        var getRemoveUser = _mapper.Map<User>(removeUserDTO);

        await _userRepository.DeleteAsync(getRemoveUser);

        return removeUserDTO;
    }

    public async Task<GetUserDTO> GetUserAsync(int id)
    {
        var getUser = await _userRepository.GetByIdAsync(id);

        var getUserDTO = _mapper.Map<GetUserDTO>(getUser);

        return getUserDTO;
    }

    public async Task<GetUserDTO> GetUserByEmailAsync(string email)
    {
        var user = await _userRepository.GetByEmailAsync(email);

        if (user is null)
            throw new NullReferenceException("user not found");

        var userDto = _mapper.Map<GetUserDTO>(user);

        return userDto;
    }

    public async Task<IEnumerable<GetAllUseresDTO>> GetAllUserAsync()
    {
        var listAllUser = await _userRepository.ListAllAsync();

        var listAllUserDTO = _mapper.Map<IEnumerable<GetAllUseresDTO>>(listAllUser);

        return listAllUserDTO;
    }

    public async Task<UpdateUserDTO> UpdateUser(UpdateUserDTO updateUserDTO)
    {
        var user = _mapper.Map<User>(updateUserDTO);
        user.ModifiedDate = DateTime.UtcNow;

        await _userRepository.UpdateAsync(user);

        return updateUserDTO;
    }

    public async Task<string> ConfirmEmail(string token)
    {
        if (!_jwtService.IsJwtToken(token))
            throw new InvalidOperationException("invalid token");

        var email = _jwtService.Decode(token);
        var user = await _userRepository.GetByEmailAsync(email);
        if (user is null)
            throw new NullReferenceException("user is null");

        if (user.IsActive)
            throw new InvalidOperationException("User is already confirmed");

        user.IsActive = true;
        await _userRepository.UpdateAsync(user);
        return "Ok";
    }

    public async Task<string> Login(LoginRequest request)
    {
        var email = request.Email.ToLower();
        var user = await _userRepository.GetByEmailAsync(email);
        if (user is null)
            throw new Exception("user not exists");

        if (!user.IsActive)
            throw new Exception("user not active");

        var isEqual = _hashService.VerifyHashedPasswordRfc2898(user.HashPassword, request.Password);
        if (isEqual)
        {
            var jwt = _jwtService.Encode(email);
            return jwt;
        }

        throw new Exception("invalid password");
    }

    public async Task LoginWithToken(string token)
    {
        if (!_jwtService.IsJwtToken(token))
            throw new Exception("invalid token");

        var email = _jwtService.Decode(token);
        var user = await _userRepository.GetByEmailAsync(email);
        if (user is null)
            throw new Exception("user not found");

        if (!user.IsActive)
            throw new Exception("user not active");
    }
}