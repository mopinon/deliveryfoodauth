using DeliveryFoodAuth.BLL.Contracts;
using DeliveryFoodAuth.BLL.Dto;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFoodAuth.Web.Controllers;

[ApiController]
[Route("[controller]")]
public class AuthController : Controller
{
    private readonly ILogger<AuthController> _logger;
    private readonly IUserService _userService;

    public AuthController(
        ILogger<AuthController> logger,
        IUserService userService)
    {
        _logger = logger;
        _userService = userService;
    }

    [HttpPost("register")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AddUserDTO))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AddUserDTO>> Register(AddUserDTO request)
    {
        try
        {
            await _userService.AddUserAsync(request);
            return new JsonResult(request);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [HttpGet("confirmEmail")]
    public async Task<ActionResult> ConfirmEmail(string token)
    {
        try
        {
            var confirmEmailResult = await _userService.ConfirmEmail(token);
            return Ok(confirmEmailResult);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpPost("login")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<string>> Login(LoginRequest request)
    {
        // try
        {
            var token = await _userService.Login(request);
            return Ok(token);
        }
        // catch (Exception e)
        {
            // return BadRequest(e.Message);
        }
    }
    
    [HttpPost("token")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<string>> Token(LoginRequest request)
    {
        // try
        {
            var token = await _userService.Login(request);
            return Ok(token);
        }
        // catch (Exception e)
        {
            // return BadRequest(e.Message);
        }
    }

    [HttpGet("loginWithToken")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> LoginWithToken([FromHeader] string token)
    {
        try
        {
            await _userService.LoginWithToken(token);
            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    // [HttpPost("resetPassword")]
    // public async Task<ActionResult<ResetPasswordResponse>> ResetPassword(ResetPasswordRequest request)
    // {
    //     var email = request.Email.ToUpper();
    //     var user = _db.Users.FirstOrDefault(u => u.Email == email);
    //     var enumerable = _db.Users.Where(e => e.IsFromUniversity == true);
    //     if (user is null)
    //     {
    //         var resetPasswordResponse = new ResetPasswordResponse(ResetPasswordStatus.UserNotExists);
    //         return new JsonResult(resetPasswordResponse);
    //     }
    //
    //     var code = StringUtil.RandomString(5).ToUpper();
    //     var resetPasswordModel = new ResetPassword
    //     {
    //         User = user,
    //         Code = code,
    //         WasUsed = false
    //     };
    //     _db.ResetPasswords.Add(resetPasswordModel);
    //     await _db.SaveChangesAsync();
    //     // var message = await PageRenderer.RenderPageToHtmlStringAsync(HttpContext.RequestServices, PathToResetPassword, code);
    //     var message = $"Код для сброса пароля: {code}";
    //     await _emailService.SendEmailAsync(email.ToLower(), "Cброс пароля", message);
    //     var passwordResponse = new ResetPasswordResponse(ResetPasswordStatus.Ok);
    //     return new JsonResult(passwordResponse);
    // }
    //
    // [HttpPost("confirmResetPassword")]
    // public async Task<ActionResult<SavePasswordResponse>> ConfirmResetPassword(SavePasswordRequest request)
    // {
    //     var code = request.Code.ToUpper();
    //     var email = request.Email.ToUpper();
    //     var user = _db.Users.FirstOrDefault(u => u.Email == email);
    //     if (user is null)
    //     {
    //         var savePasswordResponse = new SavePasswordResponse(SavePasswordStatus.UserNotExists);
    //         return new JsonResult(savePasswordResponse);
    //     }
    //
    //     var resetPasswordDto = _db.ResetPasswords
    //         .OrderBy(r => r.Id)
    //         .LastOrDefault(r => r.User == user);
    //     if (resetPasswordDto is null || resetPasswordDto.Code != code)
    //     {
    //         var savePasswordResponse = new SavePasswordResponse(SavePasswordStatus.CodeInvalid);
    //         return new JsonResult(savePasswordResponse);
    //     }
    //
    //     SavePasswordResponse passwordResponse;
    //     if (resetPasswordDto.WasUsed)
    //     {
    //         passwordResponse = new SavePasswordResponse(SavePasswordStatus.CodeWasUsed);
    //         return new JsonResult(passwordResponse);
    //     }
    //
    //     resetPasswordDto.WasUsed = true;
    //     _db.ResetPasswords.Update(resetPasswordDto);
    //     var hashPassword = _hashService.HashPasswordRfc2898(request.Password);
    //     user.HashPassword = hashPassword;
    //     _db.Users.Update(user);
    //     await _db.SaveChangesAsync();
    //     passwordResponse = new SavePasswordResponse(SavePasswordStatus.Ok);
    //     return new JsonResult(passwordResponse);
    // }
    //
    // [HttpGet("getUserInfo")]
    // public async Task<ActionResult<GetUserInfoResponse>> GetUserInfo([FromHeader] string token)
    // {
    //     GetUserInfoResponse response;
    //
    //     var isJwtToken = _jwtService.IsJwtToken(token);
    //     if (!isJwtToken)
    //     {
    //         response = new GetUserInfoResponse
    //         {
    //             Status = GetUserInfoResponseStatus.InvalidToken
    //         };
    //         return new JsonResult(response);
    //     }
    //
    //     var email = _jwtService.Decode(token);
    //     var user = _db.Users.FirstOrDefault(e => e.Email == email);
    //     if (user is null)
    //     {
    //         response = new GetUserInfoResponse
    //         {
    //             Status = GetUserInfoResponseStatus.UserNotFound
    //         };
    //         return new JsonResult(response);
    //     }
    //
    //     response = _mapper.Map<GetUserInfoResponse>(user);
    //     return new JsonResult(response);
    // }
    //
    // [HttpPost("registerWithoutEmail")]
    // public async Task<ActionResult<RegisterResponse>> RegisterWithoutEmail(
    //     [FromBody] RegisterWithoutEmailRequest request,
    //     [FromHeader] string secretKey)
    // {
    //     if (secretKey != SecretKeyProvider.SecretKey)
    //         return BadRequest("InvalidSecretKey");
    //     var email = request.Login.ToUpper();
    //     var existingUser = _db.Users.FirstOrDefault(u => u.Email == email);
    //     if (existingUser is not null && existingUser.Status != UserStatus.Unconfirmed)
    //     {
    //         var registerResponse = new RegisterResponse(RegisterStatus.UserExists);
    //         return new JsonResult(registerResponse);
    //     }
    //
    //     var encodedJwt = _jwtService.Encode(email);
    //     var hashPassword = _hashService.HashPasswordRfc2898(request.Password);
    //     var user = _mapper.Map<UserModel>(request);
    //     user.IsFromUniversity = false;
    //     user.UniversityName = string.Empty;
    //     user.Status = UserStatus.Active;
    //     user.Email = email;
    //     user.HashPassword = hashPassword;
    //     if (existingUser is not null && existingUser.Status == UserStatus.Unconfirmed)
    //     {
    //         existingUser = _mapper.Map<UserModel>(request);
    //         existingUser.IsFromUniversity = false;
    //         existingUser.UniversityName = string.Empty;
    //         existingUser.HashPassword = hashPassword;
    //         existingUser.Status = UserStatus.Active;
    //         _db.Users.Update(existingUser);
    //     }
    //     else
    //     {
    //         _db.Users.Add(user);
    //     }
    //
    //     await _db.SaveChangesAsync();
    //     var response = new RegisterResponse(RegisterStatus.Ok);
    //     return new JsonResult(response);
    // }
}